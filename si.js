function guardarProducto(descripcion, unidad, cantidad, proveedor) {
    // Validar los campos
    
    // Validar descripción (texto 60 caracteres)
    if (typeof descripcion !== 'string' || descripcion.length > 60) {
      
     
  console.error('La descripción debe ser un texto de máximo 60 caracteres.');
      return;
    }
    
    // Validar unidad (texto 10 caracteres)
    if (typeof unidad !== 'string' || unidad.length > 10) {
      console.error('La unidad debe ser un texto de máximo 10 caracteres.');
      return;
    }
    
  // Validar cantidad (numérico 4 dígitos)
    if (typeof cantidad !== 'number' || isNaN(cantidad) || cantidad < 0 || cantidad > 9999) {
      console.error('La cantidad debe ser un número de 4 dígitos (0-9999).');
      return;
    }
    
    
    }
    
   
  // Validar proveedor (texto 30 caracteres)
    if (typeof proveedor !== 'string' || proveedor.length > 30) {
      console.error('El proveedor debe ser un texto de máximo 30 caracteres.');
      return;
    }
    
    // Si todos los campos son válidos, guardar los datos del producto
    var producto = {
      descripcion: descripcion,
      unidad: unidad,
      
      cantidad
    cantidad: cantidad,
      proveedor: proveedor
    };
    
    
    console.log('Datos del producto guardados:', producto);
  guardarProducto('Camiseta', 'Unidad', 10, 'Proveedor A');




























